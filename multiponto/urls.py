from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^', include('homesite.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^batcaverna/', admin.site.urls),
]