from django.contrib import admin

from .models import Categoria, Produto

admin.site.register(Categoria)

class ProdutoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'categoria')
    search_fields = ('nome', 'categoria')
    prepopulated_fields = {'slug': ('nome',)}

admin.site.register(Produto, ProdutoAdmin)
