from django.db import models

from ckeditor.fields import RichTextField


class Categoria(models.Model):
    descricao = models.CharField(u'Descrição', max_length=50)

    def __str__(self):
        return self.descricao

    class Meta:
        db_table = 'categorias'


class Produto(models.Model):
    nome = models.CharField('Nome', max_length=100)
    descricao_curta = models.TextField(u'Descrição curta')
    descricao_completa = RichTextField(blank=True)
    informacao_adicional = RichTextField(blank=True)
    categoria = models.ForeignKey('Categoria', verbose_name='Categoria')
    imagem = models.ImageField('Imagem', upload_to='produtos/')
    slug = models.SlugField()
    preco = models.DecimalField('Preço', max_digits=12, decimal_places=2, blank=True, null=True)

    produtos_relacionados = models.ManyToManyField('Produto', verbose_name='Produtos relacionados', db_table='produtos_relacionados', blank=True)

    def __str__(self):
        return self.nome

    class Meta:
        db_table = 'produtos'
