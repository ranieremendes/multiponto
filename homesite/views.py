from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string

from .django_asynchronous_send_mail import send_mail
from .forms import ContactForm
from .models import Configuration, Slide
from produto.models import Categoria, Produto


def home(request):
    context = {}
    configuration = {}
    context['banners'] = Slide.objects.filter(show=True)
    context['produtos'] = Produto.objects.all()[:4]
    try:
        configuration = Configuration.objects.all()[0]
    except:
        pass
    if configuration:
        if configuration.contact:
            configuration.contact = configuration.contact.replace(",", "<br/>")
        context['configuration'] = configuration
    return render(request, 'home.html', context)

def sobre(request):
    context = {}
    try:
        configuration = Configuration.objects.all()[0]
    except:
        pass
    if configuration:
        if configuration.contact:
            configuration.contact = configuration.contact.replace(",", "<br/>")
        context['configuration'] = configuration
    return render(request, 'sobre.html', context)

def contato(request):
    context = {}
    configuration = {}
    try:
        configuration = Configuration.objects.all()[0]
    except Exception as e:
        return HttpResponseRedirect(reverse_lazy('home'))

    if configuration:
        if configuration.contact:
            configuration.contact = configuration.contact.replace(",", "<br/>")
        else:
            pass
    context['configuration'] = configuration

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            html_message = render_to_string(
                    'email-contact.html',
                    {
                        'name': form.cleaned_data['name'],
                        'subject': form.cleaned_data['subject'],
                        'message': form.cleaned_data['message'],
                        'email': form.cleaned_data['email']
                    }
                )
            send_mail(
                    subject='Contato do site: ' + form.cleaned_data['subject'],
                    body='',
                    from_email='contato@multipontoeacesso.com.br',
                    recipient_list=['contato@multipontoeacesso.com.br', form.cleaned_data['email']],
                    fail_silently=False,
                    html=html_message,
                    )
            messages.success(request, 'Sua mensagem foi enviada com sucesso.')
            return HttpResponseRedirect(reverse_lazy('home'))
            
    return render(request, 'contato.html', context)


def produtos(request):
    context = {}
    context['produtos'] = Produto.objects.all()
    try:
        configuration = Configuration.objects.all()[0]
        if configuration.contact:
            configuration.contact = configuration.contact.replace(",", "<br/>")
    except:
        pass
    if configuration:
        context['configuration'] = configuration
    return render(request, 'produtos.html', context)

def detalhe_produto(request, slug):
    context = {}
    context['produto'] = get_object_or_404(Produto, slug=slug)
    try:
        configuration = Configuration.objects.all()[0]
        if configuration.contact:
            configuration.contact = configuration.contact.replace(",", "<br/>")
    except:
        pass
    if configuration:
        context['configuration'] = configuration
    return render(request, 'detalhe-produto.html', context)
