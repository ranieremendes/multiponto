from django.contrib import admin

from .models import Configuration, Slide

admin.site.register(Configuration)
admin.site.register(Slide)
