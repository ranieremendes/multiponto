from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from .views import home, sobre, produtos, detalhe_produto, contato

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^sobre/$', sobre, name='sobre'),
    url(r'^produtos/$', produtos, name='produtos'),
    url(r'^detalhe-produto/(?P<slug>[^/.]+)$', detalhe_produto, name='detalhe_produto'),
    url(r'^contato/$', contato, name='contato'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
