from django.db import models

class Configuration(models.Model):
    description = models.CharField('Descrição', max_length=50, blank=False, null=False)
    contact = models.CharField('Telefones com máscara e separado por vírgula', max_length=100, blank=False, null=False)
    address = models.CharField('Endereço completo separado por vírgula', max_length=100, blank=False, null=False)
    email = models.EmailField('Email', max_length=254, blank=False, null=False)
    horary = models.CharField('Horário formatado como quer visualizar na página', max_length=50, blank=False, null=False)
    facebook = models.URLField('Url página do facebook', max_length=255, blank=True, null=True)
    instagram = models.URLField('Url página do instagram', max_length=255, blank=True, null=True)
    google_plus = models.URLField('Url página do google plus', max_length=255, blank=True, null=True)
    help_desk = models.CharField('Suporte técnico', max_length=20, blank=False, null=False)
    
    def __str__(self):
        return self.description


class Slide(models.Model):
    title = models.CharField('Título', max_length=50)
    description = models.CharField('Descrição', max_length=255)
    image = models.ImageField('Imagem', upload_to='banners/')
    show = models.BooleanField('Mostrar?', default=True)

    def __str__(self):
        return self.title
